#include "converter.h"
const size_t byte_by_pixel=sizeof (struct pixel)*8;
enum read_status bmp_header_img_from_bmp( FILE* in, struct image* img)
{
    struct bmp_header bmp;
    if(fread(&bmp,sizeof(struct bmp_header),1,in)!=1){
        return READ_INVALID_HEADER;
    }
    if(bmp.bfType!=0x4D42){return READ_INVALID_SIGNATURE;}
    if(bmp.biBitCount!=byte_by_pixel){
        return READ_INVALID_BITS;
    }
    creator(img,bmp.biWidth,bmp.biHeight,byte_by_pixel);
    struct image img2;
    img2.width=img->width;
    img2.height=img->height;
    img2.data=img->data;
    const uint64_t shift= bmp_header_padding(img->width*byte_by_pixel/8);
    for(size_t i=0;i<img->height;i++) {
        if(fread(&img->data[img->width*i],sizeof(struct pixel),img->width,in)!=img->width){
            *img=img2;
            return READ_INVALID_IMAGE;
        }
        if(fseek(in, (long)shift, SEEK_CUR)!=0){*img=img2;return READ_INVALID_IMAGE;}
    }
    return READ_OK;
}
enum write_status bmp_header_img_to_bmp(FILE* out, struct image const* img ){
    if(out==NULL||img==NULL){return WRITE_ERROR_IN_HEADER;};
    uint64_t pad=bmp_header_padding(img->width*byte_by_pixel/8);
    struct bmp_header out_header= bmp_header_create_bmp(img->width, img->height,pad,byte_by_pixel);
    out_header.bfileSize=sizeof(struct bmp_header)+out_header.biSizeImage;
    if(fwrite(&out_header,sizeof (struct bmp_header),1,out)!=1){
        return WRITE_ERROR_IN_HEADER;
    }
    for(size_t i=0;i<img->height;i++) {
        if(fwrite(&img->data[img->width*i],sizeof(struct pixel),img->width,out)!=img->width){
            return WRITE_ERROR_IN_IMG;
        }
        if(fseek(out,(long)pad,SEEK_CUR)) {
            return WRITE_ERROR_IN_PADDING;
        }
    }
    return WRITE_OK;
}
