#include "reader.h"
#include "converter.h"
#include "transformer.h"
enum massage {
    OPEN=0,
    READ,
    WRITE,
    CLOSE,
    TRANSFORM
};
const char massages[5][6][30]={
        {"Open OK\n","Open input file failed\n","Open output file failed\n"},
        {"Read OK\n","READ_INVALID_SIGNATURE\n","READ_INVALID_BITS\n","READ_INVALID_HEADER\n","READ_FAILED_TO_OPEN\n","READ_INVALID_IMAGE\n"},
        {"Write OK\n","Write failed in header\n","Write failed in image\n","Write failed in padding"},
        {"Close OK\n","Close input file failed\n","Close output file failed\n"},
        {"Transform OK","Transform failed"}
};
int main( int argc, char** argv ) {
    if (argc < 3){ fprintf(stderr,"%s","No many arguments"); return 1;};
    if (argc > 3){ fprintf(stderr,"%s","Too many arguments");return 1;};
    char *input_file=argv[1];
    char *output_file=argv[2];
    FILE *from=NULL;
    FILE *to=NULL;
    size_t open_code=open_default_file(input_file,&from,&to,output_file);
    fprintf(stderr,"%s",massages[OPEN][open_code]);
    if(open_code!=0){return 1;}
    struct image img_src;
    struct image img_chn;
    size_t read_code=bmp_header_img_from_bmp(from,&img_src);
    fprintf(stderr,"%s",massages[READ][read_code]);
    if(read_code!=0){return 1;}
    size_t transform_code=transform(&img_src,&img_chn);
    fprintf(stderr,"%s",massages[TRANSFORM][transform_code]);
    if(transform_code!=0){return 1;}
    size_t write_code=bmp_header_img_to_bmp(to,&img_chn);
    fprintf(stderr,"%s",massages[WRITE][write_code]);
    if(write_code!=0){return 1;}
    size_t close_code=close_default_file(&from,&to);
    fprintf(stderr,"%s",massages[CLOSE][close_code]);
    if(close_code!=0){return 1;};
    img_destroy(&img_src);
    img_destroy(&img_chn);
    return 0;
}
