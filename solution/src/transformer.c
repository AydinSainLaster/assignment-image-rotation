#include "transformer.h"

enum transform_status transform(struct image *img,struct image *imgc){
    if(img==NULL||imgc==NULL){
        return TRANSFORM_FAILED;
    }
    creator(imgc,img->height,img->width,sizeof (struct pixel));
    for(size_t i=0;i<imgc->height;i++){
        for(size_t j=0;j<imgc->width;j++){
            imgc->data[(i*imgc->width)+j]=img->data[(img->height-j-1)*img->width+i];
        }
    }
    return TRANSFROM_OK;
}
