//
// Created by Sainl on 06.01.2023.
//
#include "img.h"
void img_destroy(struct image *x){
    free(x->data);
}
void creator(struct image *imgc,uint64_t width,uint64_t height,size_t pixel_size){
    imgc->width=width;
    imgc->height=height;
    imgc->data=malloc(pixel_size*height*width);
}
