#include "reader.h"
#include "bmp.h"
enum open_status  open_default_file( const char*inputpath,FILE **infile,FILE **outfile,const char *outpath)
{
    *infile=fopen(inputpath,"rbe");//Try to open input file
    if(!*infile){
        return OPEN_INPUT_FILE_FAILED;
    }
    *outfile=fopen(outpath,"wbe");//Try to open output file
    if (!*outfile){
        return  OPEN_OUTPUT_FILE_FAILED;
    }
    return OPEN_IS_OK;
}
enum close_status close_default_file(FILE **from,FILE **to){
    if(fclose(*from)){//Try to close input file
        return CLOSE_INPUT_FILE_FAILED;
    }
    if(fclose(*to)){//Try to close output file
        return CLOSE_OUTPUT_FILE_FAILED;
    }
    return  CLOSE_IS_OK;
}
