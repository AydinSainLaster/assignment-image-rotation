#include "bmp.h"
#include <inttypes.h>
uint64_t bmp_header_padding(size_t width){//calculate padding
    return 4-width%4;
}
struct bmp_header bmp_header_create_bmp(size_t width,size_t height,uint64_t pad, size_t byte_by_pixel ){
    struct bmp_header res_bmp={
            .bfType=0x4D42,
            .biBitCount=24,
            .bfReserved=0,
            .biSize=40,
            .biWidth=width,
            .biHeight=height,
            .bOffBits=sizeof(struct bmp_header),
            .biPlanes=1,
            .biCompression=0,
            .biXPelsPerMeter=0,
            .biYPelsPerMeter=0,
            .biClrUsed=0,
            .biClrImportant=0,
            .biSizeImage=height*(width*byte_by_pixel/8+pad)
    };
    return res_bmp;
}

