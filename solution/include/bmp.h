#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
struct bmp_header bmp_header_create_bmp(size_t width,size_t height,uint64_t pad,size_t byte_by_pixel);
uint64_t bmp_header_padding(size_t width);
#pragma pack(pop)
#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
