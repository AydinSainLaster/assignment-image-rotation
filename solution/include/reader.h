#ifndef ASSIGNMENT_IMAGE_ROTATION_READER_H
#define ASSIGNMENT_IMAGE_ROTATION_READER_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
enum open_status {
    OPEN_IS_OK=0,
    OPEN_INPUT_FILE_FAILED,
    OPEN_OUTPUT_FILE_FAILED
};
enum close_status {
    CLOSE_IS_OK=0,
    CLOSE_INPUT_FILE_FAILED,
    CLOSE_OUTPUT_FILE_FAILED
};
enum open_status  open_default_file(const char*inputpath,FILE **infile,FILE **outfile,const char *outpath);//Function that try to open source files
enum close_status close_default_file(FILE **from,FILE **to);
#endif //ASSIGNMENT_IMAGE_ROTATION_READER_H
