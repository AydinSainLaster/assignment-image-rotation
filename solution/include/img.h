//
// Created by Sainl on 06.01.2023.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_IMG_H
#define ASSIGNMENT_IMAGE_ROTATION_IMG_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};
void img_destroy(struct image *x);
void creator(struct image *imgc, uint64_t width,uint64_t height,size_t pixel_size);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMG_H
