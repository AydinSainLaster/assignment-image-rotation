#ifndef ASSIGNMENT_IMAGE_ROTATION_CONVERTER_H
#define ASSIGNMENT_IMAGE_ROTATION_CONVERTER_H
#include "bmp.h"
#include "img.h"
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAILED_TO_OPEN,
    READ_INVALID_IMAGE
};
enum read_status bmp_header_img_from_bmp( FILE* in, struct image* img );
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR_IN_HEADER,
    WRITE_ERROR_IN_IMG,
    WRITE_ERROR_IN_PADDING
};
enum write_status bmp_header_img_to_bmp( FILE* out, struct image const* img );
#endif //ASSIGNMENT_IMAGE_ROTATION_CONVERTER_H
